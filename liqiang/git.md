#git

Git是一个开源的分布式版本控制系统，用于敏捷高效地处理任何或小或大的项目。

Git 是 Linus Torvalds 为了帮助管理 Linux 内核开发而开发的一个开放源码的版本控制软件。

软件在开发的过程，不停的有新功能和修复bug，这样软件的代码会不停的增加和变化，需要一个工具记录这个软件不同时期的任何一个版本。这个工具就是版本控制软件。

版本控制软件：里面保存的是软件的代码。

常见的版本控制软件:svn、git、CVS、Subversion 

Git 与常用的版本控制工具 CVS, Subversion 等不同，它采用了分布式版本库的方式，不必服务器端软件支持。

##github

gitHub是一个面向开源及私有软件项目的托管平台，因为只支持git 作为唯一的版本库格式进行托管，故名gitHub

github是一个基于git作为底层技术的网站平台。

Built for developers  专门为开发者而生

The world’s leading software development platform · GitHub

全世界领先的软件开发平台

GitHub is a development platform inspired by the way you work. From open source to business, you can host and review code, manage projects, and build software alongside millions of other developers.

目前github被微软收购了

官方网站：

	https://github.com/

##gitlab

GitLab 是一个用于仓库管理系统的开源项目，使用Git作为代码管理工具，并在此基础上搭建起来的web服务。安装方法是参考GitLab在GitHub上的Wiki页面。

自己的在Linux系统里搭建的一个github。

##gitbook

GitBook 是一个基于 Node.js 的命令行工具，可使用 Github/Git 和 Markdown 来制作精美的电子书。


##码云

码云专为开发者提供稳定、高效、安全的云端软件开发协作平台
无论是个人、团队、或是企业，都能够用码云实现代码托管、项目管理、协作开发

码云：中国版的github

官方网站

	https://gitee.com/

去注册一个账号：可以是QQ账号

这个账号需要牢记用户名和密码，今后每天会使用

##安装windows版的git

去官方网站下载，安装后右击，能看到下面效果，说明安装成功

![](https://i.imgur.com/xWHmV81.png)


#git和码云结合的使用案例操作

1.打开pycharm，将里面的项目上传到码云
![](https://i.imgur.com/uyST0qz.jpg)

2.进入d:\wangdalei文件夹，右击打开“git bash here”
![](https://i.imgur.com/nPv4XDF.jpg)


![](https://i.imgur.com/qOOOrg7.jpg)


3.克隆码云里的项目到本地
![](https://i.imgur.com/yGt2qYF.jpg)


	$ git clone https://gitee.com/cali/dengjianguo.git #克隆到本地电脑里
	Cloning into 'dengjianguo'...
	remote: Counting objects: 7, done.
	remote: Compressing objects: 100% (7/7), done.
	remote: Total 7 (delta 0), reused 0 (delta 0)
	Unpacking objects: 100% (7/7), done.
	
	cali@cali-PC MINGW64 /d/wangdalei
	$
	$ ls  #查看多了一个dengjianguo文件夹
	__pycache__/           dengjianguo/  djg11.py  djg2.py  djg8.py
	aa.txt                 dist/         djg12.py  djg3.py  djg9.py
	backup/                dist-bak/     djg15.py  djg5.py  setup.py
	caliosinfo5/           djg1.py       djg16.py  djg6.py  systemmonitor/
	caliosinfo5.egg-info/  djg10.py      djg19.py  djg7.py  systemmonitor.egg-info/
	
	cali@cali-PC MINGW64 /d/wangdalei  
	$ cd dengjianguo/  #进入文件夹
	
	cali@cali-PC MINGW64 /d/wangdalei/dengjianguo (master)
	$ ls
	LICENSE  README.md
	
	cali@cali-PC MINGW64 /d/wangdalei/dengjianguo (master)
	$

4.将需要上传的文件全部移动到dengjianguo文件夹下，自己在windows里操作完成

	cali@cali-PC MINGW64 /d/wangdalei/dengjianguo (master)
	$ ls -a
	./          aa.txt     djg11.py  djg2.py  djg8.py    systemmonitor/
	../         dist/      djg12.py  djg3.py  djg9.py    systemmonitor.egg-info/
	.git/       dist-bak/  djg15.py  djg5.py  LICENSE
	.gitee/     djg1.py    djg16.py  djg6.py  README.md
	.gitignore  djg10.py   djg19.py  djg7.py  setup.py
	
	cali@cali-PC MINGW64 /d/wangdalei/dengjianguo (master)
	$

5.配置好用户名和邮箱地址，用户名不是中文，是英文的那个名字

	cali@cali-PC MINGW64 /d/wangdalei/dengjianguo (master)
	$ git config --global user.name "cali" #配置用户名
	
	cali@cali-PC MINGW64 /d/wangdalei/dengjianguo (master)
	$ git config --global user.email "695811769@qq.com" #配置邮箱
	
	cali@cali-PC MINGW64 /d/wangdalei/dengjianguo (master)
	$

6.将当前目录下所有的文件和文件夹，添加到本地的版本库里缓存里

$ git add .     #添加内容到缓存库里  .代表当前文件夹里的内容

cali@cali-PC MINGW64 /d/wangdalei/dengjianguo (master)
$
7.提交内容到版本库里

	$ git commit -m  "first"  #提交到版本库
	-m 是注释，这次提交的内容是什么
	[master b0dc80a] first
	 45 files changed, 708 insertions(+)
	 create mode 100644 aa.txt
	 create mode 100644 dist-bak/caliosinfo5-1.0.2.tar.gz
	 create mode 100644 dist-bak/caliosinfo5-1.0.2/caliosinfo5-1.0.2/PKG-INFO
	 create mode 100644 dist-bak/caliosinfo5-1.0.2/caliosinfo5-1.0.2/setup.py
	 create mode 100644 dist-bak/djg13.py
	 create mode 100644 dist-bak/djg17.py
	 create mode 100644 dist-bak/djg18.py
	 create mode 100644 dist-bak/djg19.py
	 create mode 100644 dist-bak/djg4.py
	 create mode 100644 djg1.py
	 create mode 100644 djg10.py
	 create mode 100644 djg11.py
	 create
	em/student.txt
	
	cali@cali-PC MINGW64 /d/wangdalei/dengjianguo (master)
	$
8.上传到码云

	cali@cali-PC MINGW64 /d/wangdalei/dengjianguo (master)
	$ git push  #上传到码云
	Counting objects: 48, done.
	Delta compression using up to 4 threads.
	Compressing objects: 100% (45/45), done.
	Writing objects: 100% (48/48), 17.05 KiB | 256.00 KiB/s, done.
	Total 48 (delta 2), reused 0 (delta 0)
	remote: Powered by Gitee.com
	To https://gitee.com/cali/dengjianguo.git
	   173fcfd..b0dc80a  master -> master
	
	cali@cali-PC MINGW64 /d/wangdalei/dengjianguo (master)
	$

	提示：第一次上传会提醒你输入用户名和密码，用户名一定是英文的

9.到码云里查看是否有上传文件

![](https://i.imgur.com/GwUOA7L.jpg)

###总结过程：

	1.码云gitee里新建一个项目
	2.进入pycharm 里的一个项目的文件夹目录位置
	3.使用git clone 下载项目到pychar项目的文件夹目录下
	4.配置gitee里的用户名和邮箱
	
		git config  --global user.name  "cali"
		git config  --global user.email "695811769@qq.com"
	5.添加内容到缓存库
		git add .  添加我们新建的文件到git里
	6.提交内容到版本库
		git commit -m "frist commit"  #提交添加进来的文件到git
	7.推送内容到码云gitee里
		git push #将本地的添加进来的文件上传到码云里
	8.到码云里查看是否上传成功

##git的原理

- branch  分支
- master  主版本

- 稳定版本：一般是修复bug
- 开发版本：一般都是加新功能

release:发布

fix：修复

hotfixs:热修复bug

feature：特色

![](https://i.imgur.com/ZodZDBi.png)

![](https://i.imgur.com/qQad3KO.png)

**基本概念**

我们先来理解下Git 工作区、暂存区和版本库概念

工作区：就是你在电脑里能看到的目录。

暂存区：英文叫stage, 或index。一般存放在 ".git目录下" 下的index文件（.git/index）中，所以我们把暂存区有时也叫作索引（index）。

版本库：工作区有一个隐藏目录.git,是Git的版本库。

![](https://i.imgur.com/HV6PnyB.jpg)



###分支的操作


先在码云上创建分支wulongshan，然后到本地电脑里创建分支wulongshan，名字一样就可以

![](https://i.imgur.com/LNrCGW0.jpg)



![](https://i.imgur.com/QmEbIVA.jpg)


	cali@cali-PC MINGW64 /d/wangdalei/dengjianguo (master)
	$ git branch wulongshan #新建分支wulongshan
	
	cali@cali-PC MINGW64 /d/wangdalei/dengjianguo (master)
	$ git checkout wulongshan #切换分支到wulongshan，原来在master上
	Switched to branch 'wulongshan'
	
	cali@cali-PC MINGW64 /d/wangdalei/dengjianguo (wulongshan)
	$ git status #查看当前的状态
	On branch wulongshan
	nothing to commit, working tree clean
	
	cali@cali-PC MINGW64
自己在wangdalei文件夹下，新建一个yinxiang.py文件，写入一些代码

	$ git add .
	
	cali@cali-PC MINGW64 /d/wangdalei/dengjianguo (wulongshan)
	$ git commit -m "add yinxiang.py"
	[wulongshan 85df16a] add yinxiang.py
	 1 file changed, 23 insertions(+)
	 create mode 100644 yinxiang.py
	
	cali@cali-PC MINGW64 /d/wangdalei/dengjianguo (wulongshan)
	$

--set-upstream origin  上游的源（码云上的分支源头）

upstream 上游  origin  源
>默认情况下git push会上传到master分支上，上传到其他分支需要指定下分支的名字

将添加进来的文件上传到wulongshan源里

	$  git push --set-upstream origin wulongshan
	Counting objects: 2, done.
	Delta compression using up to 4 threads.
	Compressing objects: 100% (2/2), done.
	Writing objects: 100% (2/2), 259 bytes | 64.00 KiB/s, done.
	Total 2 (delta 1), reused 0 (delta 0)
	remote: Powered by Gitee.com
	To https://gitee.com/cali/dengjianguo.git
	   b0dc80a..85df16a  wulongshan -> wulongshan
	Branch 'wulongshan' set up to track remote branch 'wulongshan' from 'origin'.
	
	cali@cali-PC MINGW64 /d/wangdalei/dengjianguo (wulongshan)
	$

切换到master分支上

	$ git checkout master
	Switched to branch 'master'
	Your branch is up to date with 'origin/master'.
	
	cali@cali-PC MINGW64 /d/wangdalei/dengjianguo (master)
	$



##merge 合并分支

merge：合并

	$ git checkout master #首选切换到master分支上
	Switched to branch 'master'
	Your branch is up to date with 'origin/master'.
	
	cali@cali-PC MINGW64 /d/wangdalei/dengjianguo (master)
	$ ls
	aa.txt     djg10.py  djg16.py  djg5.py  djg9.py    systemmonitor/
	dist/      djg11.py  djg19.py  djg6.py  LICENSE    systemmonitor.egg-info/
	dist-bak/  djg12.py  djg2.py   djg7.py  README.md
	djg1.py    djg15.py  djg3.py   djg8.py  setup.py
	
	cali@cali-PC MINGW64 /d/wangdalei/dengjianguo (master)
	$ git status #查看当前的状态
	On branch master
	Your branch is up to date with 'origin/master'.
	
	nothing to commit, working tree clean
	
	cali@cali-PC MINGW64 /d/wangdalei/dengjianguo (master)

	$ git merge wulongshan  #合并分支wulongshan
	Updating b0dc80a..85df16a
	Fast-forward
	 yinxiang.py | 23 +++++++++++++++++++++++
	 1 file changed, 23 insertions(+)
	 create mode 100644 yinxiang.py
	
	cali@cali-PC MINGW64 /d/wangdalei/dengjianguo (master)

	$ git push #上传到码云
	Total 0 (delta 0), reused 0 (delta 0)
	remote: Powered by Gitee.com
	To https://gitee.com/cali/dengjianguo.git
	   b0dc80a..85df16a  master -> master
	
	cali@cali-PC MINGW64 /d/wangdalei/dengjianguo (master)
	$

最后到码云上查看master分支里的内容，是否已经包含wulongshan分支里的内容。

##项目合作

##pull requests
